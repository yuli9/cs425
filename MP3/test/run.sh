code_folder='src/'
curr_folder=$(pwd)'/'
servers=(A B C D E)
clients=(m n x y z)
pids=(0 0 0 0 0)
ports=(10001 10002 10003 10004 10005)
declare -i i=0

# shutdown servers
shutdown() {
	for port in ${ports[@]}; do
		kill -15 $(lsof -ti:$port)
	done
}
trap shutdown EXIT

# initialize servers
cp config.txt ${code_folder}/config.txt
cd $code_folder
for server in ${servers[@]}; do
	./server $server config.txt > ../server_${server}.log 2>&1 &
done

for client in ${clients[@]}; do
	timeout -s SIGTERM 5s ./client $client config.txt < ${curr_folder}input${client}.txt > ${curr_folder}output${client}.log 2>&1 &
	pids[i]=$!
	i+=1
	#./client $client config.txt < ${curr_folder}input${client}.txt > ${curr_folder}output${client}.log 2>&1 &
done
for pid in ${pids[@]}; do 
	wait $pid
done



# run 2 tests
# timeout -s SIGTERM 5s ./client m config.txt < ${curr_folder}inputm.txt > ${curr_folder}outputm.log 2>&1 &
# timeout -s SIGTERM 5s ./client n config.txt < ${curr_folder}inputn.txt > ${curr_folder}outputn.log 2>&1 &
# timeout -s SIGTERM 5s ./client x config.txt < ${curr_folder}inputx.txt > ${curr_folder}outputx.log 2>&1 &
# timeout -s SIGTERM 5s ./client y config.txt < ${curr_folder}inputy.txt > ${curr_folder}outputy.log 2>&1 &
# timeout -s SIGTERM 5s ./client z config.txt < ${curr_folder}inputz.txt > ${curr_folder}outputz.log 2>&1 &

# cd $curr_folder
# echo "Difference between your output and expected output:"
# diff output1.log expected1.txt
# diff output2.log expected2.txt
