package main

import (
	"bufio"
	"fmt"
	"log"
	"mp3/common"
	"net"
	"net/rpc"
	"os"
	"sort"
	"strconv"
	"strings"
	"sync"
	"time"
)

type FinalMsg struct {
	TID     int64
	Account string
	Server  string
}
type WriteMessage struct {
	State int // 0 for waiting, 1 for commited, 2 for aborted
	Id    int64
	Value int
}

type Account struct {
	CommitedVal int
	CommitedId  int64           // transaction id that wrote the committed value
	RTS         []int64         // list of transaction ids that have read the committed value
	TW          []*WriteMessage // list of tentative writes sorted by the corresponding transaction ids
	mu          sync.Mutex
	valid       bool
}

type Transaction struct {
	// TID  int64
	sToA map[string][]string
}

type Server struct {
	ServerId     string
	ServerPort   string
	Servers      map[string]*rpc.Client
	Accounts     map[string]*Account
	Transactions map[string]*Transaction
}

func (server *Server) rpcCall(funcname string, args *common.Command) string {
	var Reply string
	contactServer := server.Servers[args.ServerName]
	fmt.Printf("Transfer client %s command to Server %s, function: %s\n", args.ClientName, args.ServerName, args.Action)
	err := contactServer.Call(funcname, args, &Reply)
	if err != nil {
		// fmt.Printf("Error during call between server %s and server %s: %v\n", server.ServerId, args.ServerName, err)
		return "error"
	}
	return Reply
}

func (server *Server) rpcSendCommit(args *FinalMsg) string {
	var Reply string
	contactServer := server.Servers[args.Server]
	fmt.Printf("Commit: contact server %s as coordinator\n", args.Server)
	err := contactServer.Call("Server.Commit", args, &Reply)
	if err != nil {
		//fmt.Printf("Error during call between server %s and server %s: %v\n", server.ServerId, args.ServerName, err)
		return "error"
	}
	return Reply
}
func (server *Server) rpcSendPrepare(args *FinalMsg) string {
	var Reply string
	contactServer := server.Servers[args.Server]
	fmt.Printf("Prepare: contact server %s as coordinator\n", args.Server)
	err := contactServer.Call("Server.Prepare", args, &Reply)
	if err != nil {
		//fmt.Printf("Error during call between server %s and server %s: %v\n", server.ServerId, args.ServerName, err)
		return "error"
	}
	return Reply
}

func (server *Server) rpcSendAbort(args *FinalMsg) string {
	var Reply string
	contactServer := server.Servers[args.Server]
	fmt.Printf("Abort: contact server %s as coordinator\n", args.Server)
	err := contactServer.Call("Server.Abort", args, &Reply)
	fmt.Printf("rpcSemdAbort: %s\n", Reply)
	if err != nil {
		// fmt.Printf("Error during call between server %s and server %s: %v\n", server.ServerId, args.Server, err)
		return "error"
	}
	return Reply
}
func (server *Server) handleAbort(args *common.Command) {
	fmt.Printf("handle abort for %s\n", args.ClientName)
	// loop through all commands in transaction, tell the according server to abort
	for k, v := range server.Transactions[args.ClientName].sToA {
		for _, name := range v {
			arg := FinalMsg{args.TID, name, k}
			ok := server.rpcSendAbort(&arg)
			if ok != "OK" {
				fmt.Printf("error when aborting: %s\n", ok)
			}
		}

	}
}

// was select as coordinator by a client, send commands to other Servers and Reply back to client
// each separate call from cliet is a new go routine, concurrency issues?
func (server *Server) Coordinator(args *common.Command, Reply *string) error {
	fmt.Printf("selected as the coordinator by %s\n", args.ClientName)
	switch args.Action {
	case "BEGIN":
		m := make(map[string][]string)
		// server.Transactions[args.ClientName] = &Transaction{TID: args.TID, sToA: m}
		server.Transactions[args.ClientName] = &Transaction{sToA: m}
		*Reply = "OK"
	case "BALANCE":
		//check if entry exist
		ls, exist := server.Transactions[args.ClientName].sToA[args.ServerName]
		if !exist {
			ls = []string{args.AccountName}
			server.Transactions[args.ClientName].sToA[args.ServerName] = ls
		} else {
			repeat := false
			for _, name := range ls {
				if name == args.AccountName {
					repeat = true
					break
				}
			}
			if !repeat {
				ls = append(ls, args.AccountName)
				server.Transactions[args.ClientName].sToA[args.ServerName] = ls
			}
		}
		*Reply = server.rpcCall("Server.Balance", args)
		// fmt.Printf("BALANCE Reply: %s\n", *Reply)
		if *Reply == "ABORTED" || *Reply == "NOT FOUND, ABORTED" {
			server.handleAbort(args)
		}
	case "WITHDRAW":
		ls, exist := server.Transactions[args.ClientName].sToA[args.ServerName]
		if !exist {
			ls = []string{args.AccountName}
			server.Transactions[args.ClientName].sToA[args.ServerName] = ls
		} else {
			repeat := false
			for _, name := range ls {
				if name == args.AccountName {
					repeat = true
					break
				}
			}
			if !repeat {
				ls = append(ls, args.AccountName)
				server.Transactions[args.ClientName].sToA[args.ServerName] = ls
			}
		}
		*Reply = server.rpcCall("Server.Withdraw", args)
		if *Reply != "OK" {
			server.handleAbort(args)
		}
	case "DEPOSIT":
		ls, exist := server.Transactions[args.ClientName].sToA[args.ServerName]
		if !exist {
			ls = []string{args.AccountName}
			server.Transactions[args.ClientName].sToA[args.ServerName] = ls
		} else {
			repeat := false
			for _, name := range ls {
				if name == args.AccountName {
					repeat = true
					break
				}
			}
			if !repeat {
				ls = append(ls, args.AccountName)
				server.Transactions[args.ClientName].sToA[args.ServerName] = ls
			}
		}
		*Reply = server.rpcCall("Server.Deposit", args)
		if *Reply != "OK" {
			server.handleAbort(args)
		}
	case "COMMIT":
		// loop through all commands in transaction, tell the according server to commit
		for k, v := range server.Transactions[args.ClientName].sToA {
			for _, name := range v {
				fmt.Printf("cooridnator: %s sent prepare for %s\n", args.ClientName, name)
				arg := FinalMsg{args.TID, name, k}
				ok := server.rpcSendPrepare(&arg)
				//theoretically will not be abort
				if ok == "ABORT" {
					server.handleAbort(args)
					*Reply = "ABORTED"
					return nil
				}
			}
		}
		for k, v := range server.Transactions[args.ClientName].sToA {
			for _, name := range v {
				arg := FinalMsg{args.TID, name, k}
				server.rpcSendCommit(&arg) //always success
				// if ok == "ABORT" {
				// 	server.handleAbort(args)
				// 	*Reply = "ABORTED"
				// 	return nil
				// }
			}
		}
		*Reply = "COMMIT OK"
	case "ABORT":
		server.handleAbort(args)
		*Reply = "ABORTED"
	default:
		// fmt.Println("Unknown rpc request type")
	}
	//fmt.Println("replying to client")
	return nil
}

// func (server *Server) Read(accountName string, TID int64, action string) string {
func (server *Server) Read(accountName string, TID int64, action string, value int) string {
	fmt.Printf("Enter read: reading account %s with TID %d\n", accountName, TID)
	for { // use a for loop so after wait it can reapply the read rule
		account, exist := server.Accounts[accountName]
		if !exist { // cannot check balance if account does not exist
			// abort?
			if action == "DEPOSIT" {
				// return "0"
				write_success := server.Write(accountName, TID, value)
				if write_success == -1 {
					// account.mu.Unlock()
					return "WRITE FAIL"
				}
				// account.mu.Unlock()
				return "SUCCESS"
			}
			return "NOT FOUND"
		}
		var maxWrite *WriteMessage
		account.mu.Lock()
		if !account.valid {
			//created before but deleted
			if action == "DEPOSIT" {
				write_success := server.Write(accountName, TID, value)
				if write_success == -1 {
					account.mu.Unlock()
					return "WRITE FAIL"
				}
				account.mu.Unlock()
				return "SUCCESS"
			}
			account.mu.Unlock()
			return "NOT FOUND"
		}
		if TID > account.CommitedId { // greater than the committed version of object
			maxWrite = &WriteMessage{1, account.CommitedId, account.CommitedVal} //default set to commited value
			for _, w := range account.TW {
				// serach for version of object with the maximum write timestamp that is smaller than Tc
				fmt.Printf("Account %s has TW with TID %d and value %d\n", accountName, w.Id, w.Value)
				if w.Id > TID {
					break
				}
				if w.Id > maxWrite.Id && w.State != 2 {
					// fmt.Printf("found maxWrite with state %d\n", maxWrite.State)
					maxWrite = w
					fmt.Printf("found maxWrite belong to %d with state %d\n", maxWrite.Id, maxWrite.State)
				}
			}
			if maxWrite.Id == 0 { //no transaction with lower id can commit value before this read, read is not valid
				account.mu.Unlock()
				return "NOT VALID"
			}
			if maxWrite.State == 1 { // read if the found is commited, and add Tc to RTS
				exist := false
				for _, ts := range account.RTS {
					if ts == TID {
						exist = true
						break
					}
				}
				if !exist {
					account.RTS = append(account.RTS, TID) // add Tc to RTS list
					sort.Slice(account.RTS, func(i, j int) bool {
						return account.RTS[i] < account.RTS[j] // sort by ascending order
					})
				}
				// remove repeated rts?
				if action == "DEPOSIT" || action == "WITHDRAW" {
					write_success := server.Write(accountName, TID, value)
					if write_success == -1 {
						account.mu.Unlock()
						return "WRITE FAIL"
					}
					account.mu.Unlock()
					return "SUCCESS"
				}
				ret := strconv.Itoa(account.CommitedVal)
				account.mu.Unlock()
				// return "SUCCESS"
				return ret
			} else {
				if maxWrite.Id == TID { // read if the tentative write belong to the same transaction
					exist := false
					for _, ts := range account.RTS {
						if ts == TID {
							exist = true
							break
						}
					}
					if !exist {
						account.RTS = append(account.RTS, TID) // add RTS to RTS list
						sort.Slice(account.RTS, func(i, j int) bool {
							return account.RTS[i] < account.RTS[j] // sort by ascending order
						})
					}
					// ret := strconv.Itoa(maxWrite.Value)
					if action == "DEPOSIT" || action == "WITHDRAW" {
						write_success := server.Write(accountName, TID, value)
						if write_success == -1 {
							account.mu.Unlock()
							return "WRITE FAIL"
						}
						account.mu.Unlock()
						return "SUCCESS"
					}
					ret := strconv.Itoa(maxWrite.Value)
					account.mu.Unlock()
					// return "SUCCESS"
					// account.mu.Unlock()
					return ret
				}
				// else {
				// 	//fmt.Printf("waits for %d, then reapply the read rule\n", maxWrite.Id)
				// }
			}
		} else {
			// committed value is more updated than read, abort
			account.mu.Unlock()
			return "NOT VALID"
		}
		account.mu.Unlock()
		//tw is not yet committed or aborted
		time.Sleep(10 * time.Millisecond)
	}
}

// func (server *Server) Write(args *common.Command) int {
func (server *Server) Write(accountName string, TID int64, value int) int {
	fmt.Printf("Enter write: writing accuont %s with TID %d and value %d\n", accountName, TID, value)
	account, exist := server.Accounts[accountName]
	if !exist { // only deposit write can arrive here if account not already exist, withdraw will abort when read
		rts := []int64{}
		tw := []*WriteMessage{}
		account = &Account{CommitedVal: 0, CommitedId: 0, RTS: rts, TW: tw, valid: true}
		server.Accounts[accountName] = account
		fmt.Printf("account initialization: %d", server.Accounts[accountName].CommitedId)
	}
	// account.mu.Lock()
	if !account.valid { //account is created before but abort, become nonexistent
		account.valid = true
		// account.CommitedId = 0
		// account.CommitedVal = 0
		// account.RTS = []int64{}
		// account.TW = []*WriteMessage{}
	}
	if (len(account.RTS) == 0 || TID >= account.RTS[len(account.RTS)-1]) && (TID > account.CommitedId) { // perform a tentative write on D
		exist := false
		for i := 0; i < len(account.TW); i++ { // same transaction has a tw
			if account.TW[i].Id == TID {
				exist = true
				// account.TW[i].Value += args.Value
				account.TW[i].Value += value
				fmt.Printf("changed tentative value %d\n", account.TW[i].Value)
				break
			}
		}
		if !exist { // new tw
			// newtentative := WriteMessage{0, args.TID, args.Value}
			newtentative := WriteMessage{0, TID, account.CommitedVal + value}
			account.TW = append(account.TW, &newtentative)
			fmt.Printf("inserted new tentative value %d\n", value)
			sort.Slice(account.TW, func(i, j int) bool {
				return account.TW[i].Id < account.TW[j].Id // sort by id in ascending order
			})
		}
	} else {
		// abort transaction
		// account.mu.Unlock()
		fmt.Printf("TID: %d finishes write with not good\n", TID)
		return -1
	}
	// account.mu.Unlock()
	fmt.Printf("TID: %d finishes write with good\n", TID)
	return 1
}

func (server *Server) Balance(args *common.Command, Reply *string) error {
	fmt.Printf("Balance: client: %s, TID: %d, accountname: %s \n", args.ClientName, args.TID, args.AccountName)
	val := server.Read(args.AccountName, args.TID, args.Action, 0)
	if val == "NOT FOUND" {
		*Reply = "NOT FOUND, ABORTED"
		return nil
	} else if val == "NOT VALID" {
		*Reply = "ABORTED"
		return nil
	} else if val == "SUCCESS" || val == "WRITE FAIL" {
		//shouldn't enter here
		*Reply = "ABORTED"
		fmt.Printf("Balance: Unexpected stuff happend, %s.\n", val)
		return nil
	}
	*Reply = args.ServerName + "." + args.AccountName + " = " + val
	return nil
}

func (server *Server) Deposit(args *common.Command, Reply *string) error {
	fmt.Printf("Deposit: client: %s, TID: %d, accountname: %s \n", args.ClientName, args.TID, args.AccountName)
	val := server.Read(args.AccountName, args.TID, args.Action, args.Value)
	if val == "NOT VALID" || val == "NOT FOUND" {
		*Reply = "ABORTED"
		return nil
	} else if val == "WRITE FAIL" {
		*Reply = "ABORTED"
		return nil
	} else { //val == write success
		*Reply = "OK"
		return nil
	}
	// fmt.Println("trying to write the deposit")
	// value, err := strconv.Atoi(val)
	// if err != nil {
	// 	// fmt.Printf("Error at strconv in Deposit\n")
	// 	*Reply = "ABORTED"
	// 	return nil
	// }
	// value += args.Value
	// write_success := server.Write(args.AccountName, args.TID, value)
	// if write_success == -1 {
	// 	*Reply = "ABORTED"
	// 	return nil
	// } else {
	// 	*Reply = "OK"
	// 	return nil
	// }
}

func (server *Server) Withdraw(args *common.Command, Reply *string) error {
	fmt.Printf("Withdraw: client: %s, accountname: %s, TID: %d\n", args.ClientName, args.AccountName, args.TID)
	val := server.Read(args.AccountName, args.TID, args.Action, args.Value*-1)
	if val == "NOT VALID" {
		*Reply = "ABORTED"
		return nil
	} else if val == "NOT FOUND" {
		*Reply = "NOT FOUND, ABORTED"
		return nil
	} else if val == "WRITE FAIL" {
		*Reply = "ABORTED"
		return nil
	} else { //val == write success
		*Reply = "OK"
		return nil
	}
	// value, err := strconv.Atoi(val)
	// if err != nil {
	// 	// fmt.Printf("Error at strconv in Withdraw\n")
	// 	*Reply = "ABORTED"
	// 	return nil
	// }
	// value -= args.Value
	// write_success := server.Write(args.AccountName, args.TID, value)
	// if write_success == -1 {
	// 	*Reply = "ABORTED"
	// 	return nil
	// } else {
	// 	*Reply = "OK"
	// 	return nil
	// }
}

func (server *Server) Prepare(args *FinalMsg, Reply *string) error {
	fmt.Printf("Prepare: account: %s TID: %d\n", args.Account, args.TID)
	account, exist := server.Accounts[args.Account]
	idx := 0
	if !exist {
		*Reply = "ABORT" //theoretically will not enter here
		return nil
	}
	account.mu.Lock()
	if !account.valid {
		account.mu.Unlock()
		*Reply = "ABORT" //theoretically will not enter here
		return nil
	}
	for ; idx < len(account.TW); idx++ { // find its position in TW
		// fmt.Printf("Prepare: account TW tid: %d, argument tid: %d\n", account.TW[idx].Id, args.TID)
		if account.TW[idx].Id == args.TID {
			// fmt.Printf("Prepare: tid: %d index in TW: %d\n", args.TID, idx)
			break
		}
	}
	if idx == len(account.TW) {
		// fmt.Printf("TID: %d doesn't have TW\n", args.TID)
		// transaction only has read command on this account, automatically ok
		account.mu.Unlock()
		*Reply = "OK"
		return nil
	}
	if account.TW[idx].Value < 0 {
		account.mu.Unlock()
		*Reply = "ABORT"
		return nil
	}
	account.mu.Unlock()
	*Reply = "OK"
	return nil
}

func (server *Server) Commit(args *FinalMsg, Reply *string) error {
	fmt.Printf("Enter commit for account: %s, TID: %d\n", args.Account, args.TID)
	for {
		account, exist := server.Accounts[args.Account]
		if !exist {
			*Reply = "ABORT" //theoretically will not enter here
			return nil
		}
		idx := 0
		account.mu.Lock()
		fmt.Println("TW list: ")
		for i := 0; i < len(account.TW); i++ {
			fmt.Printf("Idx: %d, State: %d, Id: %d, Value: %d\n", i, account.TW[i].State, account.TW[i].Id, account.TW[i].Value)
		}
		for ; idx < len(account.TW); idx++ { // find its position in TW
			if account.TW[idx].Id == args.TID {
				break
			}
		}
		if idx == len(account.TW) {
			account.mu.Unlock()
			*Reply = "OK"
			return nil
		}
		if idx != 0 {
			if account.TW[idx-1].State == 0 {
				account.mu.Unlock()
				time.Sleep(10 * time.Millisecond)
				continue
			}
		}
		account.CommitedId = args.TID
		account.CommitedVal = account.TW[idx].Value
		//account.CommitedVal += account.TW[idx].Value
		account.TW[idx].State = 1
		//server.Accounts[args.AccountName] = account
		account.mu.Unlock()
		*Reply = "OK"
		return nil
	}
}

func (server *Server) Abort(arg *FinalMsg, Reply *string) error {
	fmt.Printf("Enter abort for account: %s tid: %d \n", arg.Account, arg.TID)
	// read should not need abort?
	acc, exist := server.Accounts[arg.Account]
	if !exist { //thereotically shouldn't enter here
		*Reply = "NOT GOOD"
		return nil
	}
	acc.mu.Lock()
	// remove tw
	if !acc.valid { //thereotically shouldn't enter here?
		acc.mu.Unlock()
		*Reply = "NOT GOOD"
		return nil
	}
	// if acc.CommitedId == 0 {
	// 	if len(acc.TW) == 1 {
	// 		// if account never committed and the aborting txn is the only one in TW, delete account
	// 		// return after delete
	// 		acc.valid = false
	// 		acc.CommitedId = 0
	// 		acc.CommitedVal = 0
	// 		acc.RTS = []int64{}
	// 		acc.TW = []*WriteMessage{}
	// 		acc.mu.Unlock()
	// 		// delete(server.Accounts, arg.Account)
	// 		*Reply = "OK"
	// 		return nil
	// 	}
	// }
	allabort := true
	for _, t := range acc.TW {
		if t.Id == arg.TID { // mark the transaction as aborted
			t.State = 2
			// break
		}
		if t.State != 2 {
			allabort = false
		}
	}
	if allabort && arg.TID == acc.TW[0].Id {
		acc.valid = false
		acc.CommitedId = 0
		acc.CommitedVal = 0
		acc.RTS = []int64{}
		acc.TW = []*WriteMessage{}
		acc.mu.Unlock()
		// delete(server.Accounts, arg.Account)
		*Reply = "OK"
		return nil
	}
	// remove rts
	i := 0
	for ; i < len(acc.RTS); i++ {
		//fmt.Printf("lenth of rts is %d\n", len(rts))
		if acc.RTS[i] == arg.TID {
			break
		}
	}
	if i+1 > len(acc.RTS)-1 { // i is the last element
		acc.RTS = acc.RTS[:i]
	} else {
		acc.RTS = append(acc.RTS[:i], acc.RTS[i+1:]...)
	}
	acc.mu.Unlock()
	*Reply = "OK"
	return nil
}

func (server *Server) ConnectServer(servername string, addr string) {
	otherServer, err := rpc.Dial("tcp", addr)
	for err != nil {
		otherServer, err = rpc.Dial("tcp", addr)
	}
	server.Servers[servername] = otherServer
	fmt.Printf("connected to server %s \n", servername)
}

func main() {
	if len(os.Args) != 3 {
		fmt.Println("Usage: ./server <server name> <config file path>")
		return
	}
	server := new(Server)
	server.ServerId = os.Args[1]
	server.Servers = make(map[string]*rpc.Client)
	server.Transactions = make(map[string]*Transaction)
	server.Accounts = make(map[string]*Account)
	rpc.Register(server)
	// fmt.Printf("server %s registered\n", server.ServerId)
	// read my port and set up listener
	file, err := os.Open(os.Args[2])
	if err != nil {
		fmt.Println("Error opening file:", err)
		return
	}
	// fmt.Println("file")
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		tokens := strings.Fields(line)
		if tokens[0] == server.ServerId {
			server.ServerPort = ":" + tokens[2]
			// fmt.Printf("port number is %s \n", server.ServerPort)
		}
		addr := tokens[1] + ":" + tokens[2]
		go server.ConnectServer(tokens[0], addr)
	}
	file.Close()
	listener, err := net.Listen("tcp", server.ServerPort)
	if err != nil {
		log.Fatalf("Failed to listen on %s: %v", server.ServerPort, err)
		//fmt.Println("failed to create listener")
	}
	defer listener.Close()
	rpc.Accept(listener)
}
