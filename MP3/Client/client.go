package main

import (
	"bufio"
	"fmt"
	"math/rand"
	"mp3/common"
	"net/rpc"
	"os"
	"strconv"
	"strings"
	"sync"
	"time"
)

type Client struct {
	clientName string
	servers    map[string]*rpc.Client
	serverName []string
	commands   []common.Command
	currTranId int64
	currCoord  string
	mu         sync.Mutex
}

func (client *Client) rpcCall(funcname string, args *common.Command) string {
	var reply string
	server := client.servers[args.Coord]
	err := server.Call(funcname, args, &reply)
	if err != nil {
		fmt.Printf("Error during call: %v\n", err)
		return "error"
	}
	// fmt.Println("received reply from coordinator")
	return reply
}

func (client *Client) readCommands() {
	// fmt.Println("type command to start:")
	input := bufio.NewReader(os.Stdin)
	for {
		command, _, err := input.ReadLine()
		if err != nil {
			// fmt.Printf("Error when reading commad from stdin: %s\n", err)
			return
		}
		// fmt.Printf("command: %s\n", command)
		// if string(command) == "BEGIN" {
		// 	client.handleBegin() // will block until receivec reply
		// } else {
		tokens := strings.Split(string(command), " ")
		if len(tokens) == 1 {
			curtime := time.Now()
			client.mu.Lock()
			if tokens[0] == "BEGIN" {
				client.currCoord = client.serverName[rand.Intn(5)]
				client.currTranId = curtime.UnixNano()
			}
			newCommand := common.Command{TID: client.currTranId, Coord: client.currCoord, Action: tokens[0], ClientName: client.clientName}
			client.commands = append(client.commands, newCommand)
			client.mu.Unlock()
		} else if len(tokens) == 2 {
			temp := strings.Split(tokens[1], ".")
			client.mu.Lock()
			newCommand := common.Command{TID: client.currTranId, Coord: client.currCoord, Action: tokens[0], ServerName: temp[0], AccountName: temp[1], ClientName: client.clientName}
			client.commands = append(client.commands, newCommand)
			client.mu.Unlock()
		} else {
			temp := strings.Split(tokens[1], ".")
			val, err := strconv.Atoi(tokens[2])
			if err != nil {
				fmt.Println("Error converting string to integer:", err)
				return
			}
			client.mu.Lock()
			newCommand := common.Command{TID: client.currTranId, Coord: client.currCoord, Action: tokens[0], ServerName: temp[0], AccountName: temp[1], Value: val, ClientName: client.clientName}
			client.commands = append(client.commands, newCommand)
			client.mu.Unlock()
		}
		// }
	}
}

// func (client *Client) handleBegin() {
// 	// use the current time as timestamp/transaction id
// 	// curtime := time.Now()
// 	// client.currTranId = curtime.UnixNano()
// 	client.mu.Lock()
// 	fmt.Printf("create new transaction with id %d\n", client.currTranId)
// 	// randomly select a server as coordinator
// 	client.currCoord = client.serverName[rand.Intn(5)]
// 	command := common.Command{TID: client.currTranId, Coord: client.currCoord, Action: "BEGIN", ClientName: client.clientName}
// 	client.mu.Unlock()
// 	reply := client.rpcCall("Server.Coordinator", &command)
// 	fmt.Println(reply)
// }

func (client *Client) sendCommands() {
	for {
		if len(client.commands) != 0 {
			client.mu.Lock()
			currCommand := client.commands[0]
			client.commands = client.commands[1:]
			client.mu.Unlock()
			// fmt.Printf("command: %s\n", currCommand.Action)
			reply := client.rpcCall("Server.Coordinator", &currCommand)
			fmt.Println(reply)
			if reply == "ABORTED" || reply == "NOT FOUND, ABORTED" || reply == "COMMIT OK" {
				return
			}
		}
	}
}

func main() {
	if len(os.Args) != 3 {
		fmt.Println("Usage: ./client <client name> <config file path>")
		return
	}
	client := new(Client)
	client.clientName = os.Args[1]
	client.servers = make(map[string]*rpc.Client)
	// Read and parse config file, store the servers
	file, err := os.Open(os.Args[2])
	if err != nil {
		fmt.Println("Error opening file:", err)
		return
	}
	defer file.Close()
	scanner := bufio.NewScanner(file)
	// fmt.Println("try to connect")
	time.Sleep(50 * time.Millisecond)
	for scanner.Scan() {
		line := scanner.Text()
		// fmt.Println(line)
		tokens := strings.Fields(line)
		addr := tokens[1] + ":" + tokens[2]
		server, err := rpc.Dial("tcp", addr)
		if err != nil {
			fmt.Printf("Error connecting to the server: %v\n", err)
		}
		client.servers[tokens[0]] = server
		client.serverName = append(client.serverName, tokens[0])
	}
	go client.readCommands()
	client.sendCommands()
}
