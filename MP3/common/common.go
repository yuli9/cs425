package common

type Command struct {
	TID         int64
	Coord       string
	Action      string
	ServerName  string
	AccountName string
	Value       int
	ClientName  string
}
