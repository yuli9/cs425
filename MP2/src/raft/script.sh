#!/bin/bash
# Function to handle the SIGINT signal (Ctrl+C)
cleanup() {
    echo "Interrupt received. Exiting..."
    exit 1
}

# Trap the SIGINT signal and call the cleanup function
trap cleanup SIGINT
# Remove output.txt if it already exists
rm -f output.txt

# Loop to run the Go program 50 times
for i in {1..50}
do
    echo "number $i"
    go test >> output.txt
done
