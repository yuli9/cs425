package raft

//
// this is an outline of the API that raft must expose to
// the service (or tester). see comments below for
// each of these functions for more details.
//
// rf = Make(...)
//   create a new Raft server.
// rf.Start(command interface{}) (index, term, isleader)
//   start agreement on a new log entry
// rf.GetState() (term, isLeader)
//   ask a Raft for its current term, and whether it thinks it is leader
// ApplyMsg
//   each time a new entry is committed to the log, each Raft peer
//   should send an ApplyMsg to the service (or tester)
//   in the same server.
//

import (
	"math/rand"
	"raft/labrpc"
	"sync"
	"sync/atomic"
	"time"
)

var hbCh chan int
var toCh chan int

// as each Raft peer becomes aware that successive log entries are
// committed, the peer should send an ApplyMsg to the service (or
// tester) on the same server, via the applyCh passed to Make(). set
// CommandValid to true to indicate that the ApplyMsg contains a newly
// committed log entry.
type ApplyMsg struct {
	CommandValid bool
	Command      interface{}
	CommandIndex int
}

// A Go object implementing a single Raft peer.
type Raft struct {
	mu    sync.RWMutex        // Lock to protect shared access to this peer's state
	peers []*labrpc.ClientEnd // RPC end points of all peers
	me    int                 // this peer's index into peers[]
	dead  int32               // set by Kill()

	// Your data here (2A, 2B).
	// Look at the paper's Figure 2 for a description of what
	// state a Raft server must maintain.
	// You may also need to add other state, as per your implementation.
	currentTerm     int // latest term server has seen
	votedFor        int // index of peer that received vote, -1 if null
	logs            []LogEntry
	commitIndex     int // index of highest log entry known to be committed
	nextIndex       []int
	matchIndex      []int
	state           int           // 0 for leader, 1 for candidate, 2 for follower
	applyCh         chan ApplyMsg // when a message is ready to be applied
	numVoteReceived int
	resetTO         bool // received message and restart countdown
}

// log entry
type LogEntry struct {
	Command interface{}
	Term    int
}

// max and min for setting the election timeout
const MIN int = 300 //all times in milliseconds
const MAX int = 500

// return currentTerm and whether this server
// believes it is the leader.
func (rf *Raft) GetState() (int, bool) {

	var term int
	var isleader bool
	// Your code here (2A).
	rf.mu.Lock()
	term = rf.currentTerm
	if rf.state == 0 {
		isleader = true
	} else {
		isleader = false
	}
	// fmt.Printf("%d %d\n", term, rf.state)
	rf.mu.Unlock()
	return term, isleader
}

// example RequestVote RPC arguments structure.
// field names must start with capital letters!
type RequestVoteArgs struct {
	// Your data here (2A, 2B).
	Term         int
	CandidateId  int
	LastLogIndex int // index of candidte's last log entry
	LastLogTerm  int // term of candidate's last log entry
}

// example RequestVote RPC reply structure.
// field names must start with capital letters!
type RequestVoteReply struct {
	// Your data here (2A).
	Term        int  // current term for candidate to update itself
	VoteGranted bool // true if received vote
}

type AppendEntryArgs struct {
	Term         int //leader's term
	LeaderId     int
	PrevLogIndex int
	PrevLogTerm  int
	Entries      []LogEntry
	LeaderCommit int // leader's commit index
}

type AppendEntryReply struct {
	Term      int  //current term, for leader to update itself
	Success   bool // true if matched
	Failedidx int
}

// example RequestVote RPC handler.
func (rf *Raft) RequestVote(args *RequestVoteArgs, reply *RequestVoteReply) {
	// Your code here (2A, 2B).
	// Read the fields in "args",
	// and accordingly assign the values for fields in "reply".
	// part 2A : empty message
	// curtime := time.Now()
	if rf.killed() {
		return
	}
	rf.mu.Lock()

	if args.Term > rf.currentTerm { // discover higher term, update my term and become follower
		rf.currentTerm = args.Term
		rf.votedFor = -1
		rf.state = 2
		// fmt.Printf("RequestVote: Server %d to follower by server %d, time: %d, election timeout: %d\n", rf.me, args.CandidateId, time.Now().UnixMilli(), rf.elecTimeout)
	}

	if args.Term < rf.currentTerm { // reject
		reply.Term = rf.currentTerm
		rf.mu.Unlock()
		reply.VoteGranted = false
		return
	}

	upToDate := false
	if len(rf.logs) > 1 {
		highestTerm := rf.logs[len(rf.logs)-1].Term
		hiestIndex := len(rf.logs) - 1
		upToDate = (highestTerm > args.LastLogTerm) || (highestTerm == args.LastLogTerm && hiestIndex > args.LastLogIndex)
	}
	if !upToDate && (rf.votedFor == -1 || rf.votedFor == args.CandidateId) {
		//if candidate is more up to date and this raft has not voted
		rf.resetTO = true
		reply.Term = rf.currentTerm
		reply.VoteGranted = true
		rf.votedFor = args.CandidateId
	} else {
		reply.Term = rf.currentTerm
		reply.VoteGranted = false
	}
	rf.mu.Unlock()
}

// example code to send a RequestVote RPC to a server.
// server is the index of the target server in rf.peers[].
// expects RPC arguments in args.
// fills in *reply with RPC reply, so caller should
// pass &reply.
// the types of the args and reply passed to Call() must be
// the same as the types of the arguments declared in the
// handler function (including whether they are pointers).
//
// The labrpc package simulates a lossy network, in which servers
// may be unreachable, and in which requests and replies may be lost.
// Call() sends a request and waits for a reply. If a reply arrives
// within a timeout interval, Call() returns true; otherwise
// Call() returns false. Thus Call() may not return for a while.
// A false return can be caused by a dead server, a live server that
// can't be reached, a lost request, or a lost reply.
//
// Call() is guaranteed to return (perhaps after a delay) *except* if the
// handler function on the server side does not return.  Thus there
// is no need to implement your own timeouts around Call().
//
// look at the comments in ../labrpc/labrpc.go for more details.
//
// if you're having trouble getting RPC to work, check that you've
// capitalized all field names in structs passed over RPC, and
// that the caller passes the address of the reply struct with &, not
// the struct itself.
func (rf *Raft) sendRequestVote(server int, args *RequestVoteArgs, reply *RequestVoteReply) bool {
	ok := rf.peers[server].Call("Raft.RequestVote", args, reply)
	if ok {
		rf.mu.Lock()
		if rf.currentTerm == args.Term {
			if reply.Term > rf.currentTerm { //discover raft with high term, step down
				rf.currentTerm = reply.Term
				rf.state = 2
				rf.resetTO = true
				// fmt.Printf("SendRequest: Server %d -> follower by server %d's reply time: %d election timeout: %d\n", rf.me, server, time.Now().UnixMilli(), rf.elecTimeout)
			} else {
				if reply.VoteGranted {
					rf.numVoteReceived += 1
				}
			}
		}
		rf.mu.Unlock()
	}
	return ok
}

// appendentries handler
func (rf *Raft) AppendEntries(args *AppendEntryArgs, reply *AppendEntryReply) {
	if rf.killed() {
		return
	}
	// set default values
	reply.Failedidx = 0
	msgs := make([]ApplyMsg, 0)
	rf.mu.Lock()
	lastlogidx := len(rf.logs) - 1
	AppendValid := (len(rf.logs) > args.PrevLogIndex) && (args.PrevLogIndex >= 1 && rf.logs[args.PrevLogIndex].Term == args.PrevLogTerm)
	rf.resetTO = true
	if rf.currentTerm > args.Term { //the "leader" seems obsolete
		reply.Success = false
		reply.Term = rf.currentTerm
		rf.mu.Unlock()
		return
	} else {
		reply.Term = rf.currentTerm
		rf.currentTerm = args.Term
		rf.state = 2
		if !(args.PrevLogIndex < 1 || AppendValid) { //fail consistency check
			reply.Success = false
			if len(rf.logs) <= args.PrevLogIndex { //missing entries
				for idx := lastlogidx - 1; idx > 0; idx-- {
					if rf.logs[idx].Term != rf.logs[lastlogidx].Term {
						reply.Failedidx = idx
						break
					}
				}
			} else { //
				idx := args.PrevLogIndex
				for idx > rf.commitIndex && rf.logs[idx].Term == rf.logs[args.PrevLogIndex].Term { // re append logs in the last term if possible
					idx -= 1
				}
				reply.Failedidx = idx
			}
			rf.mu.Unlock()
			return
		}
		reply.Success = true
		rf.logs = append(rf.logs[:args.PrevLogIndex+1], args.Entries...)
		reply.Failedidx = len(rf.logs) - 1
	}
	if args.LeaderCommit > rf.commitIndex {
		oldindex := rf.commitIndex
		if args.LeaderCommit >= len(rf.logs)-1 {
			rf.commitIndex = len(rf.logs) - 1
		} else {
			rf.commitIndex = args.LeaderCommit
		}
		for idx := oldindex + 1; idx <= rf.commitIndex; idx++ {
			msgs = append(msgs, ApplyMsg{true, rf.logs[idx].Command, idx})
		}
	}
	rf.mu.Unlock()
	for _, msg := range msgs {
		rf.applyCh <- msg
	}
}

func (rf *Raft) sendEntry(i int, args *AppendEntryArgs, reply *AppendEntryReply) bool {
	ok := rf.peers[i].Call("Raft.AppendEntries", args, reply)
	if rf.killed() {
		return ok
	}
	msgs := make([]ApplyMsg, 0)
	rf.mu.Lock()
	if args.Term == rf.currentTerm { // term is same as when raft sent out message, so reply valid to use
		if ok {
			if reply.Term > rf.currentTerm { // discover higher term, step down
				rf.currentTerm = reply.Term
				rf.resetTO = true
				rf.state = 2
				rf.mu.Unlock()
				return ok
			}
		} else {
			rf.mu.Unlock()
			return ok
		}
		if rf.currentTerm == reply.Term {
			rf.nextIndex[i] = reply.Failedidx + 1
			rf.matchIndex[i] = reply.Failedidx
		}
		newidxavail := false
		newidx := 0
		match_count := 1
		for i := len(rf.logs) - 1; i > rf.commitIndex; i-- {
			// newCommitIndex = rf.commitIndex + i
			match_count = 1
			// if newCommitIndex > len(rf.logs)-1 {
			// 	continue
			// }
			for idx := range rf.peers {
				if rf.matchIndex[idx] >= i {
					match_count += 1
				}
			}
			if rf.logs[i].Term == rf.currentTerm && match_count >= len(rf.peers)/2+1 {
				newidxavail = true
				newidx = i
				break
			}
		}
		if newidxavail {
			for idx := rf.commitIndex + 1; idx <= newidx; idx++ { // apply messges that are newly marked as comitted
				msgs = append(msgs, ApplyMsg{true, rf.logs[idx].Command, idx})
				// rf.applyCh <- msg
			}
			rf.commitIndex = newidx
		}
	}
	rf.mu.Unlock()
	for _, msg := range msgs {
		rf.applyCh <- msg
	}
	return ok
}

// the service using Raft (e.g. a k/v server) wants to start
// agreement on the next command to be appended to Raft's log. if this
// server isn't the leader, returns false. otherwise start the
// agreement and return immediately. there is no guarantee that this
// command will ever be committed to the Raft log, since the leader
// may fail or lose an election. even if the Raft instance has been killed,
// this function should return gracefully.
//
// the first return value is the index that the command will appear at
// if it's ever committed. the second return value is the current
// term. the third return value is true if this server believes it is
// the leader.
func (rf *Raft) Start(command interface{}) (int, int, bool) {
	index := -1
	term := -1

	// Your code here (2B).]
	rf.mu.Lock()
	isLeader := true
	if rf.state != 0 {
		isLeader = false
		rf.mu.Unlock()
		return index, term, isLeader
	} else {
		logentry := LogEntry{command, rf.currentTerm}
		rf.logs = append(rf.logs, logentry)
		index = len(rf.logs) - 1
		term = rf.currentTerm
	}
	rf.mu.Unlock()
	return index, term, isLeader
}

// the tester doesn't halt goroutines created by Raft after each test,
// but it does call the Kill() method. your code can use killed() to
// check whether Kill() has been called. the use of atomic avoids the
// need for a lock.
//
// the issue is that long-running goroutines use memory and may chew
// up CPU time, perhaps causing later tests to fail and generating
// confusing debug output. any goroutine with a long-running loop
// should call killed() to check whether it should stop.
func (rf *Raft) Kill() {
	atomic.StoreInt32(&rf.dead, 1)
	// Your code here, if desired.
}

func (rf *Raft) killed() bool {
	z := atomic.LoadInt32(&rf.dead)
	return z == 1
}

// the service or tester wants to create a Raft server. the ports
// of all the Raft servers (including this one) are in peers[]. this
// server's port is peers[me]. all the servers' peers[] arrays
// have the same order. applyCh is a channel on which the
// tester or service expects Raft to send ApplyMsg messages.
// Make() must return quickly, so it should start goroutines
// for any long-running work.
func Make(peers []*labrpc.ClientEnd, me int,
	applyCh chan ApplyMsg) *Raft {
	rf := &Raft{}
	rf.peers = peers
	rf.me = me

	// Your initialization code here (2A, 2B).
	rf.currentTerm = 0
	rf.votedFor = -1
	rf.logs = make([]LogEntry, 1)
	rf.commitIndex = 0
	rf.state = 2 // start as follower
	rf.applyCh = applyCh
	hbCh = make(chan int)
	toCh = make(chan int)
	rf.numVoteReceived = 0
	rf.nextIndex = make([]int, len(peers))
	rf.matchIndex = make([]int, len(peers))
	for idx := range rf.peers {
		rf.nextIndex[idx] = 1
	}
	go rf.server(toCh, hbCh)
	go rf.HeartBeat(hbCh)
	go rf.TimeOut(toCh)
	return rf
}

func (rf *Raft) TimeOut(toCh chan int) {
	for {
		if rf.killed() {
			continue
		}
		timeout := int64(MIN + rand.Intn(MAX-MIN))
		for {
			time.Sleep(time.Duration(timeout) * time.Millisecond)
			rf.mu.Lock()
			if rf.resetTO {
				rf.resetTO = false
				rf.mu.Unlock()
				timeout = int64(MIN + rand.Intn(MAX-MIN))
				continue
			} else {
				rf.mu.Unlock()
				break
			}
		}
		toCh <- 1
	}
}

// an infinte loop for raft server to send messages (only if they are leader or candidate)
func (rf *Raft) HeartBeat(hbCh chan int) {
	for {
		if rf.killed() {
			continue
		}
		time.Sleep(70 * time.Millisecond)
		hbCh <- 1
	}
}

func (rf *Raft) server(toCh chan int, hbCh chan int) {
	// elecTimeout := int64(MIN + rand.Intn(MAX-MIN))
	for {
		if rf.killed() {
			continue
		}
		// time.Sleep(10 * time.Millisecond) // reduce loop
		timeout := false
		hb := false
		select {
		case <-toCh:
			timeout = true
		case <-hbCh:
			hb = true
		}
		rf.mu.Lock()
		switch rf.state {
		case 2: //follower
			if timeout { //election time out
				rf.currentTerm += 1
				rf.numVoteReceived = 1
				rf.votedFor = rf.me
				rf.state = 1
				for index := range rf.peers {
					if index != rf.me {
						requestvote := RequestVoteArgs{rf.currentTerm, rf.me, len(rf.logs) - 1, -1}
						if len(rf.logs) > 1 {
							requestvote.LastLogTerm = rf.logs[len(rf.logs)-1].Term
						}
						reply := RequestVoteReply{}
						go rf.sendRequestVote(index, &requestvote, &reply)
					}
				}
				timeout = false
			} else {
				rf.numVoteReceived = 0
				rf.votedFor = -1
			}
		case 1: //candidate
			if timeout {
				//election time out
				rf.currentTerm += 1
				rf.votedFor = rf.me
				rf.numVoteReceived = 1
				for index := range rf.peers {
					if index != rf.me {
						requestvote := RequestVoteArgs{rf.currentTerm, rf.me, len(rf.logs) - 1, -1}
						if len(rf.logs) > 1 {
							requestvote.LastLogTerm = rf.logs[len(rf.logs)-1].Term
						}
						reply := RequestVoteReply{}
						go rf.sendRequestVote(index, &requestvote, &reply)
					}
				}
				timeout = false
			} else {
				if rf.numVoteReceived >= (len(rf.peers)/2 + 1) {
					rf.state = 0
					rf.numVoteReceived = 0
					//initialize nextIndex
					for index := range rf.nextIndex {
						if index != rf.me {
							rf.nextIndex[index] = len(rf.logs)
						} else {
							rf.nextIndex[index] = 0
						}
						rf.matchIndex[index] = 0
					}
					for index := range rf.peers {
						if index != rf.me {
							firstArgs := AppendEntryArgs{rf.currentTerm, rf.me, rf.nextIndex[index] - 1, -1, nil, rf.commitIndex}
							firstReply := AppendEntryReply{}
							if (len(rf.logs) - 1) >= rf.nextIndex[index] {
								firstArgs.Entries = rf.logs[rf.nextIndex[index]:]
							}
							if rf.nextIndex[index] > 1 {
								firstArgs.PrevLogTerm = rf.logs[rf.nextIndex[index]-1].Term
							}
							// fmt.Printf("APPEND TO %d term: %d, leader id: %d, prevlogidx: %d, prevlogterm: %d, lenof entry: %d, leadercommit: %d\n", index, rf.currentTerm, rf.me, firstArgs.PrevLogIndex, firstArgs.PrevLogTerm, len(firstArgs.Entries), firstArgs.LeaderCommit)
							go rf.sendEntry(index, &firstArgs, &firstReply)
						}
					}
				}
			}
		case 0: //leader
			if hb {
				for index := range rf.peers {
					if index != rf.me {
						hbArgs := AppendEntryArgs{rf.currentTerm, rf.me, rf.nextIndex[index] - 1, -1, nil, rf.commitIndex}
						hbReply := AppendEntryReply{}
						if rf.nextIndex[index] <= len(rf.logs)-1 {
							hbArgs.Entries = rf.logs[rf.nextIndex[index]:]
						}
						if rf.nextIndex[index] > 1 {
							hbArgs.PrevLogTerm = rf.logs[rf.nextIndex[index]-1].Term
						}
						//fmt.Printf("APPEND TO %d term: %d, leader id: %d, prevlogidx: %d, prevlogterm: %d, lenof entry: %d, leadercommit: %d\n", index, rf.currentTerm, rf.me, hbArgs.PrevLogIndex, hbArgs.PrevLogTerm, len(hbArgs.Entries), hbArgs.LeaderCommit)
						go rf.sendEntry(index, &hbArgs, &hbReply)
					}
				}
				hb = false
			}
		}
		rf.mu.Unlock()
	}
}
