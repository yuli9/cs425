package main

import (
	"bufio"
	"container/heap"
	"encoding/gob"
	"fmt"
	"math"
	priorityQueue "mp1/lib"
	"net"
	"os"
	"sort"
	"strconv"
	"strings"
	"sync"
	"time"
)

type Map_value struct {
	Msg_content  string
	Num_proposed int
} //value inside map

type Msg struct {
	Msg_type    int //0 normal msg, 1 proposed priority, 2 final priority
	Priority    float32
	Msg_id      string
	Msg_content string
	Node_id     int
} //msg over network

var priority_count float32 //keeps track of the max priority seen/proposed so far
var pc_lock sync.Mutex

var num_nodes int
var nn_lock sync.Mutex

var pqueue = make(priorityQueue.PriorityQueue, 0)
var pq_lock sync.Mutex

var mp map[string]Map_value
var m_lock sync.Mutex

type net_conn_t struct {
	Enc *gob.Encoder
	Id  int
}

var net_conn_enc []net_conn_t
var net_conn_lock sync.Mutex

var queue []string
var queue_lock sync.Mutex

func receiver(c net.Conn, ch chan int, self int) {
	ch <- 1
	dec := gob.NewDecoder(c)
	var n_id int //which node this receiver is communicating to
	for {
		var m Msg
		for m.Node_id == 0 { //didn't receive anything
			err := dec.Decode(&m)
			// fmt.Printf("recv_loop: %d %s\n", m.Node_id, m.Content)
			if err != nil {
				// fmt.Println("Node Failed: ", err)
				//this node is down, reduce the total number of nodes
				pq_lock.Lock()
				pqueue.Invalidate(strconv.Itoa(n_id))
				// pqueue.Dump()
				pq_lock.Unlock()

				nn_lock.Lock()
				num_nodes--
				// fmt.Printf("n_id: %d, self: %d, num_nodes: %d  ", n_id, self, num_nodes)
				nn_lock.Unlock()
				//this node is down, mark the transactions from this node(will never receive the final priority) undeliverable
				//remove from net_conn array
				net_conn_lock.Lock()
				for i, enc := range net_conn_enc {
					if enc.Id == n_id {
						net_conn_enc[i] = net_conn_enc[len(net_conn_enc)-1]
						net_conn_enc = net_conn_enc[:len(net_conn_enc)-1]
						break
					}
				}
				// fmt.Printf("net_conn_len: %d\n", len(net_conn_enc))
				net_conn_lock.Unlock()
				//compare with num proposed, determine final priority and send
				return
			}
		}
		// fmt.Printf("receiver: %d %f %s %s %d\n", m.Msg_type, m.Priority, m.Msg_id, m.Msg_content, m.Node_id)
		n_id = m.Node_id
		//reply
		if m.Msg_type == 0 { // received normal message
			// update max proposed priority
			pc_lock.Lock()
			proposedP := float32(math.Floor(float64(priority_count))) + 1 + float32(self)*0.1
			priority_count = proposedP
			pc_lock.Unlock()

			//update map
			m_val := Map_value{Msg_content: m.Msg_content, Num_proposed: 0}
			m_lock.Lock()
			mp[m.Msg_id] = m_val
			m_lock.Unlock()

			// push new message and proposed priority to queue
			newItem := &priorityQueue.Item{Value: m.Msg_id, Priority: proposedP, Deliverable: 0}
			pq_lock.Lock()
			heap.Push(&pqueue, newItem)
			pq_lock.Unlock()
			// send message
			newMsg := Msg{
				Msg_type:    1,
				Node_id:     self,
				Msg_id:      m.Msg_id,
				Msg_content: "",
				Priority:    proposedP,
			}
			net_conn_lock.Lock()
			for _, enc_element := range net_conn_enc {
				if enc_element.Id == n_id {
					err := enc_element.Enc.Encode(newMsg)
					if err != nil { //node is down
						// fmt.Println("error reply\n")
						// return
					}
				}
			}
			net_conn_lock.Unlock()
		} else if m.Msg_type == 1 { // received proposed priority
			maxproposed := m.Priority
			var curr *priorityQueue.Item

			nn_lock.Lock()
			cur_nodes := num_nodes
			nn_lock.Unlock()

			is_final := false

			pq_lock.Lock()

			// if is_final {
			// 	pqueue.Modify(m.Msg_content, 1)
			// }
			if pqueue.Find(m.Msg_id) == nil {
				// fmt.Printf("message id: %s, content: %s, type: %d, node id: %d\n", m.Msg_id, m.Msg_content, m.Msg_type, m.Node_id)
				// pqueue.Dump()
				pq_lock.Unlock()
				continue
			} else {
				curr = pqueue.Find(m.Msg_id).(*priorityQueue.Item)
			}
			if curr.Priority > maxproposed {
				maxproposed = curr.Priority
			} else {
				pqueue.Update(m.Msg_id, maxproposed)
			}
			m_lock.Lock()
			temp := mp[m.Msg_id]
			temp.Num_proposed += 1
			mp[m.Msg_id] = temp
			if temp.Num_proposed >= cur_nodes-1 { // is final
				is_final = true
				// fmt.Printf("temp.numproposed: %d\n", temp.Num_proposed)
				pqueue.Modify(m.Msg_id, 1)
			}
			m_lock.Unlock()
			pq_lock.Unlock()

			if is_final {
				msg := Msg{
					Msg_type:    2,
					Msg_id:      m.Msg_id,
					Node_id:     self,
					Msg_content: "",
					Priority:    maxproposed,
				}
				b_multicast(msg, self)
			}
		} else if m.Msg_type == 2 { // received final priority
			pq_lock.Lock()
			pqueue.Update(m.Msg_id, m.Priority)
			pqueue.Modify(m.Msg_id, 1)
			pq_lock.Unlock()

			pc_lock.Lock()
			if m.Priority > priority_count {
				priority_count = m.Priority
			}
			pc_lock.Unlock()
		}
	}
}

func b_multicast(msg Msg, self_id int) {
	net_conn_lock.Lock()
	for _, enc := range net_conn_enc {
		if enc.Id != self_id {
			// fmt.Printf("send: %d %s to: %d\n", msg.Node_id, msg.Msg_content, enc.Id)
			// fmt.Printf("b_multicast: %d\n", enc.Id)
			err := enc.Enc.Encode(msg)
			if err != nil {
				// fmt.Printf("%d\n", enc.Id)
				// fmt.Println("Encode error")
			}
		}
	}
	net_conn_lock.Unlock()
}

func sender(c chan int, self int) {
	counter := 0
	count := 0
	for {
		if <-c == 1 {
			counter++
		}
		if counter == (num_nodes-1)*2 {
			break
		}
	}
	for {
		is_update := false
		var transaction string
		queue_lock.Lock()
		if len(queue) > 0 {
			is_update = true
			transaction = queue[0]
			queue = queue[1:]
		}
		queue_lock.Unlock()
		// fmt.Printf("msg_id: %s\n", msg_id)
		if is_update {
			count += 1
			msg_id := strconv.Itoa(self) + "." + strconv.Itoa(count)
			pc_lock.Lock()
			proposedP := float32(math.Floor(float64(priority_count))) + 1 + float32(self)*0.1
			priority_count = proposedP
			pc_lock.Unlock()
			new_msg := Msg{
				Msg_type:    0,
				Priority:    proposedP,
				Msg_id:      msg_id,
				Msg_content: transaction,
				Node_id:     self,
			}
			newItem := &priorityQueue.Item{Value: msg_id, Priority: proposedP, Deliverable: 0}
			pq_lock.Lock()
			heap.Push(&pqueue, newItem)
			// fmt.Println("Sender pqueue dump: \n")
			// pqueue.Dump()
			pq_lock.Unlock()
			m_lock.Lock()
			mp[msg_id] = Map_value{Msg_content: transaction, Num_proposed: 0}
			m_lock.Unlock()
			// fmt.Printf("sent: msg_id: %s\n", msg_id)
			b_multicast(new_msg, self)
		}
	}
}

func readingtransaction(self int, f1 *os.File) {
	reader := bufio.NewReader(os.Stdin)
	count := 0
	for {
		count += 1
		msg_id := strconv.Itoa(self) + "." + strconv.Itoa(count)
		input, err := reader.ReadString('\n')
		if err != nil {
			// fmt.Println("Finish Sending\n")
			f1.Close()
			break
		}
		currtimeStamp := float64(time.Now().Unix()) + float64(time.Now().Nanosecond())/1e9
		writemsg := fmt.Sprintf("%f,%s\n", currtimeStamp, msg_id)
		f1.WriteString(writemsg)
		queue_lock.Lock()
		queue = append(queue, input)
		queue_lock.Unlock()
	}
}

func application(f2 *os.File, self int) {
	var mp_app map[string]int //account: balance
	mp_app = make(map[string]int)
	for {
		is_deliver := false
		pq_lock.Lock()
		element := pqueue.Peek()
		var transaction *priorityQueue.Item
		if element != nil {
			e := element.(*priorityQueue.Item)
			if e.Deliverable == 1 {
				is_deliver = true
				transaction = heap.Pop(&pqueue).(*priorityQueue.Item)
			} else if e.Deliverable == -1 {
				is_deliver = false
				transaction = heap.Pop(&pqueue).(*priorityQueue.Item)
				// fmt.Printf("App1: Msg_id: %s Priority: %f Deliverability: %d\n", e.Value, e.Priority, e.Deliverable)
			} else {
				nn_lock.Lock()
				curr_nn := num_nodes
				nn_lock.Unlock()
				m_lock.Lock()
				if mp[e.Value].Num_proposed >= curr_nn-1 {
					// fmt.Printf("App2: Msg_id: %s Priority: %f Deliverability: %d, num_proposed: %d\n", e.Value, e.Priority, e.Deliverable, mp[e.Value].Num_proposed)
					e.Deliverable = 1
					msg := Msg{
						Msg_type:    2,
						Msg_id:      e.Value,
						Node_id:     self,
						Msg_content: "",
						Priority:    e.Priority,
					}
					b_multicast(msg, self)
				}
				m_lock.Unlock()
				// pqueue.Dump()
				transaction = nil
				is_deliver = false
			}
		}
		pq_lock.Unlock()
		if is_deliver {
			msg_id := transaction.Value
			currtimeStamp := float64(time.Now().Unix()) + float64(time.Now().Nanosecond())/1e9
			writemessage := fmt.Sprintf("%f,%s\n", currtimeStamp, msg_id)
			f2.WriteString(writemessage)
			m_lock.Lock()
			t := strings.TrimSuffix(mp[msg_id].Msg_content, "\n")
			m_lock.Unlock()
			tokens := strings.Split(t, " ")
			if tokens[0] == "DEPOSIT" {
				value, err := strconv.Atoi(tokens[2])
				if err != nil {
					fmt.Println("strconv failed")
				} else {
					balance, ok := mp_app[tokens[1]]
					if ok {
						balance += value
						mp_app[tokens[1]] = balance
					} else {
						mp_app[tokens[1]] = value
					}
				}
			} else if tokens[0] == "TRANSFER" {
				userFrom := tokens[1]
				userTo := tokens[3]
				amount, err := strconv.Atoi(tokens[4])
				if err != nil {
					fmt.Println("strconv failed")
				} else {
					balanceFrom, ok := mp_app[userFrom]
					valid := false
					if ok {
						if balanceFrom >= amount {
							balanceFrom -= amount
							mp_app[userFrom] = balanceFrom
							valid = true
						}
					}
					if valid {
						balanceTo, ok2 := mp_app[userTo]
						if ok2 {
							balanceTo += amount
							mp_app[userTo] = balanceTo
						} else {
							mp_app[userTo] = amount
						}
					}
				}
			} else {
				fmt.Printf("%s", t)
				fmt.Println("Invalid transaction")
			}
			keys := make([]string, 0, len(mp_app))
			for k := range mp_app {
				keys = append(keys, k)
			}
			sort.Strings(keys)
			fmt.Printf("BALANCES")
			for _, k := range keys {
				if mp_app[k] != 0 {
					fmt.Printf(" %s:%d", k, mp_app[k])
				}
			}
			fmt.Printf("\n")
		}
	}
}

func connection(addr string, port string, c chan int, id int) {
	// fmt.Println("reached here 1")
	conn, err := net.Dial("tcp", fmt.Sprintf("%s:%s", addr, port))
	for err != nil {
		conn, err = net.Dial("tcp", fmt.Sprintf("%s:%s", addr, port))
	}
	enc := gob.NewEncoder(conn)
	net_conn_lock.Lock()
	net_conn_enc = append(net_conn_enc, net_conn_t{enc, id})
	net_conn_lock.Unlock()
	c <- 1
}

func main() {
	f1, err1 := os.Create("output_sendmsg.txt")
	if err1 != nil {
		fmt.Println("file open fail")
	}
	f2, err2 := os.Create("output_delivermsg.txt")
	if err2 != nil {
		fmt.Println("file open fail")
	}
	if len(os.Args) < 3 {
		fmt.Println("./mp1_node [identifier] [configuration file]")
		return
	}

	id := os.Args[1]
	f, err := os.Open(os.Args[2])
	if err != nil {
		fmt.Printf("Cannot open config file: %v", err)
		return
	}
	fscanner := bufio.NewScanner(f)
	counter := 0
	self := 0
	port := ""
	c := make(chan int)
	for fscanner.Scan() {
		if counter == 0 {
			num_nodes, err = strconv.Atoi(fscanner.Text())
			counter += 1
		} else if counter <= num_nodes {
			tokens := strings.Split(fscanner.Text(), " ")
			if tokens[0] == id {
				port = ":" + tokens[2]
				self = counter
			} else {
				// fmt.Println("here !")
				ip, err := net.LookupIP(tokens[1])
				// fmt.Println("here 2")
				if err != nil {
					fmt.Printf("%s:%s %v\n", tokens[1], tokens[2], err)
					return
				}
				go connection(ip[0].String(), tokens[2], c, counter)
			}
			counter += 1
		} else {
			fmt.Printf("Invalid file input, exceeding the number of nodes.\n")
		}
	}
	mp = make(map[string]Map_value)
	queue = make([]string, 0) //buffer between stdin and sending channel
	heap.Init(&pqueue)
	go application(f2, self)
	go readingtransaction(self, f1)
	go sender(c, self)
	ln, err := net.Listen("tcp", port)
	if err != nil {
		fmt.Println(err)
		return
	}

	for {
		conn, err := ln.Accept()
		if err != nil {
			return
		}
		go receiver(conn, c, self)
	}
	// return
}
