package priorityQueue

// source: https://pkg.go.dev/container/heap

import (
	"container/heap"
	"fmt"
	"strings"
)

// An Item is something we manage in a Priority queue.
type Item struct {
	Value    string  // The Value of the item; message id.
	Priority float32 // The Priority of the item in the queue.
	// The Index is needed by update and is maintained by the heap.Interface methods.
	Index       int // The Index of the item in the heap.
	Deliverable int //0: proposed stage, 1: deliverable, -1: undeliverable
}

// A PriorityQueue implements heap.Interface and holds Items.
type PriorityQueue []*Item

func (pq PriorityQueue) Len() int { return len(pq) }

func (pq PriorityQueue) Less(i, j int) bool {
	// We want Pop to give us the highest, not lowest, priority so we use greater than here.
	return pq[i].Priority < pq[j].Priority
}

func (pq PriorityQueue) Swap(i, j int) {
	pq[i], pq[j] = pq[j], pq[i]
	pq[i].Index = i
	pq[j].Index = j
}

func (pq *PriorityQueue) Push(x any) {
	n := len(*pq)
	item := x.(*Item)
	item.Index = n
	*pq = append(*pq, item)
}

func (pq *PriorityQueue) Pop() any {
	old := *pq
	n := len(old)
	item := old[n-1]
	old[n-1] = nil  // avoid memory leak
	item.Index = -1 // for safety
	*pq = old[0 : n-1]
	return item
}

// func (pq *PriorityQueue) CondPop() any {
// 	old := *pq
// 	n := len(old)
// 	item := old[n-1]
// 	if item.Deliverable == 0 {
// 		return item
// 	}
// 	ret := heap.Pop(pq)
// 	return ret
// }

func (pq *PriorityQueue) Invalidate(NodeID string) {
	for _, item := range *pq {
		tokens := strings.Split(item.Value, ".")
		if tokens[0] == NodeID {
			item.Deliverable = -1
		}
	}
}

func (pq *PriorityQueue) Dump() {
	fmt.Println("*****PQ DUMP*****")
	for _, item := range *pq {
		fmt.Printf("Msg_id: %s Priority: %f Deliverability: %d\n", item.Value, item.Priority, item.Deliverable)
	}
	fmt.Println("*****PQ DUMP END*****")
}

func (pq *PriorityQueue) Peek() any {
	if len(*pq) != 0 {
		return (*pq)[0]
	} else {
		return nil
	}
}

func (pq *PriorityQueue) Modify(Value string, d int) {
	for _, item := range *pq {
		if (item.Value == Value) && (item.Deliverable != -1) {
			item.Deliverable = d
			break
		}
	}
}

// update modifies the Priority and Value of an Item in the queue.
func (pq *PriorityQueue) Update(Value string, newPriority float32) {
	for _, item := range *pq {
		if item.Value == Value {
			item.Priority = newPriority
			heap.Fix(pq, item.Index)
			break
		}
	}
}

func (pq *PriorityQueue) Find(Value string) any {
	for _, item := range *pq {
		if item.Value == Value {
			return item
		}
	}
	return nil
}
