import pandas as pd
import plotly.express as px
import sys

case = int(sys.argv[1])
node = int(sys.argv[2])
# start time
filename = "data/" + str(case) + "/output_sendmsg" + str(case) + "_" + str(1) + ".txt"
startdata = pd.read_csv(filename, header=None, names=['time','id'],  dtype={'id': 'string', 'time': 'float'})
for i in range(2,node+1):
    filename = "data/" + str(case) + "/output_sendmsg" + str(case) + "_" + str(i) + ".txt"
    datax = pd.read_csv(filename, header=None, names=['time','id'],  dtype={'id': 'string', 'time': 'float'})
    startdata = startdata.append(datax, ignore_index=True)
# end times
filename = "data/" + str(case) + "/output_delivermsg" + str(case) + "_" + str(1) + ".txt"
enddata = pd.read_csv(filename, header=None, names=['time','id'],  dtype={'id': 'string', 'time': 'float'})
for i in range(2,node+1):
    filename = "data/" + str(case) + "/output_delivermsg" + str(case) + "_" + str(i) + ".txt"
    datax = pd.read_csv(filename, header=None, names=['time','id'],  dtype={'id': 'string', 'time': 'float'})
    enddata = enddata.append(datax, ignore_index=True)

time = {}
for index, row in startdata.iterrows():
    time[row['id']] = (row['time'], 0)
for index, row in enddata.iterrows():
    s, e = time[row['id']]
    if e < row['time']:
        e = row['time']
    time[row['id']] = (s,e)

diff = []
for key in time:
    s, e = time[key]
    if e != 0:
        diff.append(e-s)

diff.sort()
df = pd.DataFrame({'time': diff})
df['cdf'] = df['time'].rank(method='max', pct=True)

fig = px.line(df, x='time', y='cdf', title='CDF of Transaction Processing Time',
              labels={'time': 'Time/seconds', 'cdf': 'Percentile'})

# Show the plot
fig.write_image('plot' + str(case) + '.png')

