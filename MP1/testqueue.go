package main

import (
	"container/heap"
	"fmt"
	priorityQueue "mp1/lib"
)

var pq = make(priorityQueue.PriorityQueue, 0)

func main() {
	// pq := make(priorityQueue.PriorityQueue, 0)
	heap.Init(&pq)
	item1 := &priorityQueue.Item{Value: "1.1", Priority: 1.1, Deliverable: 0}
	item2 := &priorityQueue.Item{Value: "1.2", Priority: 2.1, Deliverable: 1}
	item3 := &priorityQueue.Item{Value: "2.1", Priority: 2.2, Deliverable: 0}
	item4 := &priorityQueue.Item{Value: "2.2", Priority: 3.2, Deliverable: 1}
	item5 := &priorityQueue.Item{Value: "1.3", Priority: 1.5, Deliverable: 0}
	item6 := &priorityQueue.Item{Value: "2.3", Priority: 4.2, Deliverable: 1}
	item7 := &priorityQueue.Item{Value: "3.1", Priority: 5.3, Deliverable: 0}
	item8 := &priorityQueue.Item{Value: "3.2", Priority: 2.3, Deliverable: 1}
	heap.Push(&pq, item3)
	heap.Push(&pq, item4)
	heap.Push(&pq, item1)
	heap.Push(&pq, item2)
	heap.Push(&pq, item5)
	heap.Push(&pq, item6)
	heap.Push(&pq, item7)
	heap.Push(&pq, item8)
	// for pq.Len() > 0 {
	// 	item := pq.Peek().(*priorityQueue.Item)
	// 	fmt.Printf("%f:%s\n", item.Priority, item.Value)
	// }
	// pq.Update("b", 1.5)
	// fmt.Printf("updated\n")
	// wait := 0
	// for pq.Len() > 0 {
	// 	if wait == 6 {
	// 		pq.Modify("a", 1)
	// 	}
	// 	if wait == 10 {
	// 		break
	// 	}
	// 	//item := pq.CondPop().(*priorityQueue.Item)
	// 	//item := heap.Pop(&pq).(*priorityQueue.Item)
	// 	item := pq.Peek().(*priorityQueue.Item)
	// 	fmt.Printf("len of pq is %d\n", pq.Len())
	// 	fmt.Printf("%f:%s:%d\n", item.Priority, item.Value, item.Deliverable)
	// 	wait++
	// }
	test := pq.Peek().(*priorityQueue.Item)
	test1 := pq.Find("1.3").(*priorityQueue.Item)
	test1.Deliverable = 1
	test2 := heap.Pop(&pq).(*priorityQueue.Item)
	fmt.Printf("test: Value: %s, Priority: %f, Deliverable: %d, index: %d\n", test.Value, test.Priority, test.Deliverable, test.Index)
	fmt.Printf("test1: Value: %s, Priority: %f, Deliverable: %d, index: %d\n", test1.Value, test1.Priority, test1.Deliverable, test1.Index)
	fmt.Printf("test2: Value: %s, Priority: %f, Deliverable: %d, index: %d\n", test2.Value, test2.Priority, test2.Deliverable, test2.Index)
	// pq.Invalidate(strconv.Itoa(2))
	// pq.Dump()
	return
}
