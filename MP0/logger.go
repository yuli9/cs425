package main

import (
    "fmt"
    "os"
    "net"
    "bufio"
    "strings"
    "time"
	"unsafe"
	"strconv"
)

func handleConnection(c net.Conn, f *os.File, nodeName string) {
	for {
		netData,err := bufio.NewReader(c).ReadString('\n')
		if err != nil {
			currtime := float64(time.Now().Unix()) + float64(time.Now().Nanosecond())/1e9
			fmt.Printf("%f - %s disconnected\n", currtime, nodeName)
			break
		}
		temp := strings.TrimSpace(string(netData))
		fmt.Printf("%s\n", temp)
		tokens := strings.Split(temp, " ")
		oldtimeStamp, err := strconv.ParseFloat(tokens[0], 64)
		currtimeStamp := float64(time.Now().Unix()) + float64(time.Now().Nanosecond())/1e9
		delay := currtimeStamp - oldtimeStamp
		tot_byte := unsafe.Sizeof(temp)
		writemesg := fmt.Sprintf("%f %f %d\n", currtimeStamp, delay, tot_byte)
		f.WriteString(writemesg)
	}
	c.Close()
}

func main() {
	port := ":" + os.Args[1]
	f,err := os.Create("output.txt")
	ln, err := net.Listen("tcp", port)
	if err != nil {
		fmt.Println(err)
		return
	}

	for {
		conn, err := ln.Accept()
		if err != nil {
			return
		}
		netData,err := bufio.NewReader(conn).ReadString('\n')
		nodeName := strings.TrimSpace(string(netData))
		currtimeStamp := float64(time.Now().Unix()) + float64(time.Now().Nanosecond())/1e9
		fmt.Printf("%f - %s connected\n", currtimeStamp, nodeName)

		go handleConnection(conn, f, nodeName)
	}
	f.Close()
}
