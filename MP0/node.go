package main

import (
	"bufio"
	"fmt"
	"net"
	"os"
	"strings"
)

func main() {
	if len(os.Args) != 4 {
		fmt.Println("Usage: go run main.go <server_ip> <server_port>")
		return
	}

	nodeName := os.Args[1]
	serverIP := os.Args[2]
	serverPort := os.Args[3]

	// Connect to the server
	conn, err := net.Dial("tcp", fmt.Sprintf("%s:%s", serverIP, serverPort))
	if err != nil {
		fmt.Printf("Error connecting to the server: %v\n", err)
		return
	}
	connMessage := fmt.Sprintf("%s\n", nodeName)
	conn.Write([]byte(connMessage))

	reader := bufio.NewReader(os.Stdin)

	for {
		input, err := reader.ReadString('\n')
		if err != nil {
			fmt.Println("Finish Sending\n")
			break
		}
		tokens := strings.Split(input, " ")
		message := fmt.Sprintf("%s %s %s", tokens[0], nodeName, tokens[1])
		conn.Write([]byte(message))
	}
	fmt.Println("Connection closed.")
	conn.Close()
	return
}
