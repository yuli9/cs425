import plotly.graph_objs as go
import numpy as np
from math import floor

file = open("output.txt")
data = file.readlines()
time = []
min_delay = []
max_delay = []
med_delay = []
perc90 = []
bw = []
time0 = float(data[0].split()[0])
base_t = time0
t = [float(data[0].split()[1])]
byte_count = int(data[0].split()[2])
def med(l):
    return l[int(len(l)/2)]
def percentage90(l):
    n = len(l)
    idx = floor(0.9*n)
    return l[idx-1]

for row in data[1:]:
    x, y, z = row.split()
    if float(x) - time0 >= 0.8:
        time.append(time0 - base_t)
        t.sort()
        min_delay.append(min(t))
        max_delay.append(max(t))
        med_delay.append(med(t))
        perc90.append(percentage90(t))
        bw.append((byte_count*8)/1000/(float(x)-time0))
        time0 = float(x)
        t.clear()
        t.append(float(y))
        byte_count = int(z)
    else:
        if float(y) < 0:
            t.append(0)
        else:
            t.append(float(y))
        byte_count += int(z)
fig = go.Figure()
fig.add_trace(go.Line(x = time, y = min_delay, name="min"))
fig.add_trace(go.Line(x = time, y = max_delay, name="max"))
fig.add_trace(go.Line(x = time, y = med_delay, name="median"))
fig.add_trace(go.Line(x = time, y = perc90, name="90 percentile"))
fig.update_layout(title="Delay", xaxis_title="time(s)", yaxis_title="delay(s)")
fig1=go.Figure()
fig1.add_trace(go.Line(x = time, y = bw))
fig1.update_layout(title="Bandwidth", xaxis_title="time(s)", yaxis_title="bandwidth(Kb/s)")
fig.show()
fig1.show()